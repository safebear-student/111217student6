Feature: User Management

  User story: (optional)
  In order to manage users
  As an administrator
  I need CRUS permissions to user accounts

  Rules:
  - non-admin users have not permissions oer other accounts
  -There are two types of non-admin accounts:
  --Risk managers
  --Asset managers
  -Non-admin users must only be able to see risks or assets in their Group
  -the administrator must be able to reset user's passwords
  -When a account is created, a user must e forced to rest their password on first logon

  Question
  -Do admin users need access to all accounts or only to the accounts in their group?

  To do
  - Force reset of password on account creation

  Domain language
  Group = users are defined by which group they belong to
  CRUD Create, read, Update, Delete
  Administrator permissions = access to CRUD risks, users and assets for all groups
  risk_management permissions = only able to CRUD risks
  asset-Management permission = Only able to CRUD assets

  Background:
    Given a user called simon with administrator permissions and password S@feb3ar
    And a user called tom with risk_management permissions and password S@feB3ar

    @high-impact
    Scenario Outline: the administrator checks a user's details
    When simon is logged in with password S@feb3ar
    Then he is able to view <users>'s account
      Examples:
        | users|
        |tom|

    @high-risk
    @todo
      Scenario Outline: A user's password is reset by the administrator
      Given a <user> has lost his password
      When simon is logged in with password S@feB3ar
      Then simon can rest <user>'s password
      Examples:
        | user |
        |tom   |